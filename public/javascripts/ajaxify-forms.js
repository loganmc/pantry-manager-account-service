document.addEventListener('DOMContentLoaded', (event) => {
  const ajaxForms = document.querySelectorAll('form.ajax-form');
  ajaxForms.forEach((ajaxForm) => {
    if (ajaxForm.querySelector('.list-group') == null) {
      const listGroup = document.createElement('div');
      listGroup.classList.add('list-group');
      ajaxForm.append(listGroup);
    }
    ajaxForm.addEventListener('submit', async (e) => {
      try {
        e.preventDefault();
        // Get form data from html
        const data = {};
        ajaxForm.querySelectorAll('input, select').forEach((input) => {
          data[input.name] = input.value;
        });
        const request = await axios({
          url: ajaxForm.action,
          method: ajaxForm.method,
          data: data,
        });

        const successItem = document.createElement('div');
        successItem.classList.add('list-group-item', 'list-group-item-success');
        successItem.innerHTML = JSON.stringify(request.data);
        ajaxForm.querySelector('.list-group').append(successItem);
      } catch (error) {
        const errorItem = document.createElement('div');
        errorItem.classList.add('list-group-item', 'list-group-item-danger');
        errorItem.innerHTML = error;
        ajaxForm.querySelector('.list-group').append(errorItem);
      }
    });
  });
});
