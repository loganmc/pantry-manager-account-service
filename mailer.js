const mailgun = require('mailgun-js');
const DOMAIN = 'mg.souschef.tech';
const mg = mailgun({
  apiKey: process.env.MAILGUN_API_KEY,
  domain: DOMAIN,
});
module.exports.sendEmail = (message) => {
  return new Promise(async (resolve, reject) => {
    try {
      const data = {
        from: 'Souschef Kitchen Assistant <noreply@souschef.tech>',
        to: message.to,
        subject: message.subject,
        text: message.text,
        html: message.html,
      };
      resolve(await mg.messages().send(data));
    } catch (error) {
      reject(error);
    }
  });
};
