const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const User = require('../models/User');
const Notification = require('../models/Notification');
const Auth = require('../auth');
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {title: process.env.SERVICE_NAME});
});

router.post('/signup', async (req, res, next) => {
  try {
    const email = req.body.email;
    const password = req.body.password;

    const hash = bcrypt.hashSync(password, 8);

    const newUser = await new User({
      email: email,
      password: hash,
    }).save();
    console.log('new user', newUser);
    const token = await Auth.generateToken({
      user: newUser,
    });
    res.status(201).json({
      token: token,
    }).end();
  } catch (error) {
    next(error);
  }
});

router.post('/signin', async (req, res, next) => {
  try {
    const email = req.body.email;
    const password = req.body.password;

    const isMatch = await Auth.authenticate(email, password);

    if (isMatch) {
      const user = await User.findOne({email: email});
      const token = await Auth.generateToken({
        user: user,
      });

      res.status(200).json({
        token: token,
      }).end();
      return;
    }

    res.status(401).json({
      message: 'Sign in failed',
    }).end();
  } catch (error) {
    next(error);
  }
});

router.get('/user', async (req, res, next) => {
  try {
    const token = req.headers['authorization'];
    console.log('TOKEN', token);
    const decodedToken = await Auth.verifyToken(token);
    console.log('decoded token', decodedToken);
    if (decodedToken == null) {
      throw new Error('Something went wrong');
    }
    const userId = decodedToken.user._id;
    const user = await User.findById(userId);

    if (user == null) {
      throw new Error('User not found');
    }

    res.status(200).json({user: user}).end();
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.post('/notification', async (req, res, next) => {
  try {
    const token = req.headers['authorization'];
    const decodedToken = await Auth.verifyToken(token);
    if (decodedToken == null) {
      res.status(401).end();
      return;
    }
    const userId = decodedToken.user._id;
    const notification = new Notification({
      subject: req.body.subject,
      content: {
        html: req.body.html,
        text: req.body.text,
      },
      user: userId,
    });
    await notification.save();
    // notification.send();
    res.status(200).json({
      message: 'Notification Added to Queue',
    });
  } catch (error) {
    next(error);
  }
});

router.get('/notification/send', async (req, res, next) => {
  try {
    let msg = '';
    const notifications = await Notification.find({isSent: false});
    notifications.forEach(async (notification) => {
      msg += await notification.send();
    });
    res.json({msg: JSON.stringify(notifications)}).end();
  } catch (error) {
    next(error);
  }
});

module.exports = router;
