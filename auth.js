const User = require('./models/User');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const bcrypt = require('bcryptjs');

module.exports.generateToken = (payload, signOptions) => {
  return new Promise(async (resolve, reject) => {
    try {
      const privateKey = fs.readFileSync(process.env.JWT_PRIVATE_KEY);
      const token = jwt.sign(payload, privateKey, {
        algorithm: 'RS256',
        expiresIn: '2 days',
      });
      resolve(token);
    } catch (error) {
      reject(error);
    }
  });
};

module.exports.authenticate = async (email, password) => {
  try {
    if (email == null || password == null) return false;
    const userWithSameEmail = await User.findOne({email: email});

    if (userWithSameEmail == null) {
      return false;
    }
    const hash = userWithSameEmail.password;
    const passwordMatchesHash = bcrypt.compareSync(password, hash);
    return passwordMatchesHash;
  } catch (error) {
    throw error;
  }
};

module.exports.verifyToken = async (token) => {
  return new Promise(async (resolve, reject) => {
    try {
      const privateKey = fs.readFileSync(process.env.JWT_PUBLIC_KEY);
      const decoded = jwt.verify(token, privateKey);
      resolve(decoded);
    } catch (error) {
      reject(error);
    }
  });
};
