const mongoose = require('mongoose');
const User = require('./User');
const Mailer = require('../mailer');
const Schema = mongoose.Schema;

const NotificationSchema = new Schema({
  subject: String,
  content: {
    html: String,
    text: String,
  },
  dateCreated: {
    type: Date,
    default: Date.now(),
  },
  dateUpdated: {
    type: Date,
    default: Date.now(),
  },
  dateSent: Date,
  isSent: {
    type: Boolean,
    default: false,
  },
  user: {
    type: mongoose.Types.ObjectId,
    ref: 'User',
  },
});


NotificationSchema.methods.send = async function() {
  console.log('notification.send()');
  try {
    const user = await User.findById(this.user);
    console.log('user', user);
    const deliveryMethod = user.notificationDeliveryMethod;
    if (deliveryMethod == 'EMAIL') {
      console.log('Abousta send email');
      await Mailer.sendEmail({
        to: user.email,
        subject: this.subject,
        text: this.content.text,
        html: this.content.html,
      });
      console.log('email sent');
    }
    console.log('NOTIFICATIONS SEND', this);
    this.dateSent = Date.now();
    this.isSent = true;
    return await this.save();
  } catch (error) {
    throw (error);
  }
};

const Notification = mongoose.model('Notification', NotificationSchema);


module.exports = Notification;
