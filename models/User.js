const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
  email: String,
  password: String,
  phone: String,
  dateCreated: {
    type: Date,
    default: Date.now(),
  },
  dateUpdated: {
    type: Date,
    default: Date.now(),
  },
  notificationDeliveryMethod: {
    type: String,
    default: 'EMAIL',
  },
  notificationInterval: {
    type: String,
    default: 'ASAP',
  },
});

const User = mongoose.model('User', UserSchema);

module.exports = User;
